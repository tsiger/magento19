<?php
class MY_Checkoutfields_Block_Adminhtml_Checkoutfields extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_checkoutfields';
    $this->_blockGroup = 'checkoutfields';
    $this->_headerText = Mage::helper('checkoutfields')->__('Item Manager');
    $this->_addButtonLabel = Mage::helper('checkoutfields')->__('Add Item');
    parent::__construct();
  }
}