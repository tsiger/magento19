<?php

class MY_Checkoutfields_Block_Adminhtml_Checkoutfields_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('checkoutfieldsGrid');
      $this->setDefaultSort('checkoutfields_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('checkoutfields/checkoutfields')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('id', array(
          'header'    => Mage::helper('checkoutfields')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'checkoutfields_id',
      ));

      $this->addColumn('productId',array(
          'header' => Mage::helper('checkoutfields')->__('Product Id'),
          'width'     => '70px',
          'index'     => 'product_id',
      ));
      
      $this->addColumn('OrderId',array(
          'header' => Mage::helper('checkoutfields')->__('Order Id'),
          'width'     => '70px',
          'index'     => 'order_id',
      ));
      
      $this->addColumn('LabeProduct',array(
          'header' => Mage::helper('checkoutfields')->__('Label product'),
          'index'     => 'label_product',
      ));
      
      $this->addColumn('LabelGlobal',array(
          'header' => Mage::helper('checkoutfields')->__('Label global'),
          'index'     => 'label_global',
      ));
      
      $this->addColumn('File',array(
          'header' => Mage::helper('checkoutfields')->__('File'),
          'index'     => 'file',
      ));

      $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('checkoutfields')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('checkoutfields')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('checkoutfields')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('checkoutfields')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('checkoutfields_id');
        $this->getMassactionBlock()->setFormFieldName('checkoutfields');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('checkoutfields')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('checkoutfields')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('checkoutfields/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('checkoutfields')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('checkoutfields')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}