<?php

class MY_Checkoutfields_Block_Adminhtml_Checkoutfields_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('checkoutfields_form', array('legend'=>Mage::helper('checkoutfields')->__('Item information')));
     
      $fieldset->addField('product_id', 'text', array(
          'label'     => Mage::helper('checkoutfields')->__('Product id'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'product_id',
      ));

      $fieldset->addField('order_id', 'text', array(
          'label'     => Mage::helper('checkoutfields')->__('Order id'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'order_id',
      ));

      $fieldset->addField('label_product', 'text', array(
          'label'     => Mage::helper('checkoutfields')->__('Label product'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'label_product',
      ));


      $fieldset->addField('label_global', 'text', array(
          'label'     => Mage::helper('checkoutfields')->__('Label global'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'label_global',
      ));
      
      $fieldset->addField('file', 'text', array(
          'label'     => Mage::helper('checkoutfields')->__('File'),
          'class'     => 'required-entry',
          'required'  => false,
          'name'      => 'file',
      ));
     
      if ( Mage::getSingleton('adminhtml/session')->getCheckoutfieldsData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getCheckoutfieldsData());
          Mage::getSingleton('adminhtml/session')->setCheckoutfieldsData(null);
      } elseif ( Mage::registry('checkoutfields_data') ) {
          $form->setValues(Mage::registry('checkoutfields_data')->getData());
      }
      return parent::_prepareForm();
  }
}