<?php

class MY_Checkoutfields_Block_Adminhtml_Checkoutfields_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('checkoutfields_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('checkoutfields')->__('Item Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('checkoutfields')->__('Item Information'),
          'title'     => Mage::helper('checkoutfields')->__('Item Information'),
          'content'   => $this->getLayout()->createBlock('checkoutfields/adminhtml_checkoutfields_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}