<?php

class MY_Checkoutfields_Block_Adminhtml_Checkoutfields_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'checkoutfields';
        $this->_controller = 'adminhtml_checkoutfields';
        
        $this->_updateButton('save', 'label', Mage::helper('checkoutfields')->__('Save Item'));
        $this->_updateButton('delete', 'label', Mage::helper('checkoutfields')->__('Delete Item'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('checkoutfields_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'checkoutfields_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'checkoutfields_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('checkoutfields_data') && Mage::registry('checkoutfields_data')->getId() ) {
            return Mage::helper('checkoutfields')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('checkoutfields_data')->getTitle()));
        } else {
            return Mage::helper('checkoutfields')->__('Add Item');
        }
    }
}