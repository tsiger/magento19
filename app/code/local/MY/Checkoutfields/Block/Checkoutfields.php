<?php
class MY_Checkoutfields_Block_Checkoutfields extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getCheckoutfields()     
     { 
        if (!$this->hasData('checkoutfields')) {
            $this->setData('checkoutfields', Mage::registry('checkoutfields'));
        }
        return $this->getData('checkoutfields');
        
    }
}