<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('checkoutfields')};
CREATE TABLE {$this->getTable('checkoutfields')} (
  `checkoutfields_id` int(11) unsigned NOT NULL auto_increment,
  `product_id` int(11) unsigned NOT NULL,
  `order_id` int(11) unsigned NOT NULL,
  `label_product` varchar(255) NOT NULL default '',
  `label_global` varchar(255) NOT NULL default '',
  PRIMARY KEY (`checkoutfields_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->getConnection()
    ->addColumn($installer->getTable('checkoutfields'),
        'file',
        array(
            'type'    => Varien_Db_Ddl_Table::TYPE_VARCHAR,
            'length'  => 255,
        )
    );

$installer->endSetup(); 