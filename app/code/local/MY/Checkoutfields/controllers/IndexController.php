<?php
class MY_Checkoutfields_IndexController extends Mage_Core_Controller_Front_Action
{
    
    public function indexAction()
    {
        $this->loadLayout();     
        $this->renderLayout();
    }
    
    private function setLabel($name, $value) {
        Mage::getSingleton('core/session')->{"set_".$name}($value);
        if ($result = Mage::getSingleton('core/session')->{"get_".$name}()) {
            echo 'Value '.$result.' is set';
        }
        else {
            echo 'Sorry we cant set data';
        }
    }
    
    public function listAction() {
        $this->loadLayout();     
        $this->renderLayout();
    }
    
    public function ajaxAction() {
        if ($label_global_value = Mage::app()->getRequest()->getParam('label_global')) {
            $this->setLabel('label_global', $label_global_value);
        }
        if ($label_product_value = Mage::app()->getRequest()->getParam('label_product')) {
            $this->setLabel('label_product', $label_product_value);
        }
    }
    
    public function uploadfileAction() {
        if(isset($_FILES['docname']['name']) && $_FILES['docname']['name'] != '') {
            try
            {   
                $path = Mage::getBaseDir().DS.'media'.DS.'customer_documents'.DS;  //desitnation directory     
                $fname = $_FILES['docname']['name']; //file name                        
                $uploader = new Varien_File_Uploader('docname'); //load class
                $uploader->setAllowedExtensions(array('doc','pdf','txt','docx')); //Allowed extension for file
                $uploader->setAllowCreateFolders(true); //for creating the directory if not exists
                $uploader->setAllowRenameFiles(false); //if true, uploaded file's name will be changed, if file with the same name already exists directory.
                $uploader->setFilesDispersion(false);
                $uploader->save($path,$fname); //save the file on the specified path
                Mage::getSingleton('core/session')->set_docname($fname);
                $message = 'File has been uploaded successfully!';
            }
            catch (Exception $e)
            {
                Mage::getSingleton('core/session')->set_docname('');
                $message = 'Error Message: '.$e->getMessage();
            }
        }

        //add Info Message
        Mage::getSingleton('checkout/session')->addNotice($message);
        session_write_close(); //THIS LINE IS VERY IMPORTANT!
        $this->_redirect('checkout/cart');
        
    }
    
    public function deletefileAction() {
        $fname = Mage::getSingleton('core/session')->get_docname();
        $fpath = $path = Mage::getBaseDir().DS.'media'.DS.'customer_documents'.DS.$fname;
        if (isset($_POST['deletefile']) && $fname) {
            try {
                unlink($fpath);
                Mage::getSingleton('core/session')->set_docname('');
                $message = 'File has been deleted successfully!';
            }
            catch (Exception $e) {
                Mage::getSingleton('core/session')->set_docname('');
                $message = 'Error Message: '.$e->getMessage();
            }
        }
        
        Mage::getSingleton('checkout/session')->addNotice($message);
        session_write_close();
        $this->_redirect('checkout/cart');
    }
    
}