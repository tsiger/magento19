<?php

class MY_Checkoutfields_Model_Mysql4_Checkoutfields_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('checkoutfields/checkoutfields');
    }
}