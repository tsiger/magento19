<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MY_Checkoutfields_Model_Checkout_Observer{
    
    function saveAditionalData1($observer){
        
        $order = $observer->getOrder();
        $orderId = $order->getId();
        
        //get all items (with child products)
        //$items = $order->getAllItems();
        
        //get something
        //$oItems = $order->getItemsCollection();
        
        //get only parent products (configurable products)
        $orderItems = $order->getAllVisibleItems();
        
        $label_global = Mage::getSingleton('core/session')->get_label_global();
        $label_product = Mage::getSingleton('core/session')->get_label_product();
        $fname = Mage::getSingleton('core/session')->get_docname();
        
        $model = Mage::getModel('checkoutfields/checkoutfields');
        
        foreach ($orderItems as $item) {
            
            // get type of product ('configurable' or 'simple')
            //$product = Mage::getModel('catalog/product')->load($item->getProductId());
            //$type = $product->getTypeId();
            
            for ($i=0; $i < $item->getQtyOrdered(); $i++) {
                $model->setData(array(
                    'product_id' => $item->getProductId(),
                    'order_id' => $orderId,
                    'label_product' => $label_product,
                    'label_global' => $label_global,
                    'file' => $fname,
                ));
                $model->save();
            }
        }
    }
    /*
    function saveAditionalData($observer){
        
        $orderIds = $observer->getData('order_ids');
        $order = Mage::getModel('sales/order')->load($orderIds[0]);
        $items = $order->getAllItems();
        $oItems = $order->getItemsCollection();
        $orderItems = $order->getAllVisibleItems();
        
        $label_global = Mage::getSingleton('core/session')->get_label_global();
        $label_product = Mage::getSingleton('core/session')->get_label_product();
        $fname = Mage::getSingleton('core/session')->get_docname();
        
        $model = Mage::getModel('checkoutfields/checkoutfields');
        
        foreach ($orderItems as $item) {
            $product = Mage::getModel('catalog/product')->load($item->getProductId());
            $type = $product->getTypeId();
            if ($type !== 'simple') {
                $model->setData(array(
                    'product_id' => $item->getProductId(),
                    'order_id' => $orderIds[0],
                    'label_product' => $label_product,
                    'label_global' => $label_global,
                    'file' => $fname,
                ));
                $model->save();
            }
        }
    }
    */
}